@extends('layouts.app')

@section('content')
	<h1>Create Post</h1>

	<form method="post" action="/posts">
        @csrf
        <div class="form-group">
            <label label for="title"> Title</label>
            <input type="text" name="title">

        </div>

        <div class="form-group">
                <label for="title">Body</label>
                <textarea class="form-control" name="body" rows="10" id="body"></textarea>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Submit</button>
    </form>
@endsection

<script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>